import 'dotenv-flow/config';
import { server } from './server';


const port = process.env.PORT || 8000;


server.listen(port, ()=> {
    console.log('listening on port '+port);
});


//Créer un token quasi infini
// console.log(generateToken({
//     email: 'test@test.com',
//     id: 5,
//     role: 'user'
// }, 99999));