
## How to Use
1. Créer une base de données et importer le fichier dedans (exemple: `mysql -u simplon -p node_authentication < db.sql`)
2. `npm i`
3. Créer un fichier .env avec une DATABASE_URL pour la database créée
4. Ajouter une clef JWT_SECRET dans le .env avec une valeur quelconque (c'est le clef qui servira à encrypter le token)
5. Si on veut vérifier si ça marche : `npm tests`
6. `npm start` 
